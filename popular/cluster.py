#!/usr/bin/python -tt
# This module implements the BIC clustering technique based on the k_mean algorithm
__author__ = "Viet Trinh"  # vqtrinh@ucsc.edu

import numpy as np


# BIC cluster the dataSet from given intial centroids.
# Return a succesful k-cluster with its final centroids
# The format of the clusters is a dictionary where
#    key is the i-th cluster
#    value is an np.array of feature vectors
def bic(dataSet, intialCentroids, maxIter):
    centroids = intialCentroids
    prevCentroids = None
    itr = 0
    clusters = {}

    while not should_stop(prevCentroids, centroids, itr, maxIter):
        prevCentroids = centroids
        clusters = determine_cluster(dataSet, centroids)
        centroids = reevaluate_centroids(clusters)
        itr += 1

    return [clusters, centroids]


# Determine when to stop the calculation.
#   either maximum iteration is reached
#   or no update on centroids (clusters have converged)
def should_stop(prevCentroids, centroids, itr, maxIter):
    if itr > maxIter: return True
    return np.array(prevCentroids).all() == np.array(centroids).all()


# Cluster the dataSet for given centroids
def determine_cluster(dataSet, centroids):
    # Cluster is a diction in the form:
    # { 0:[(str,arr),(str,arr),(str,arr)...],
    #   1:[(str,arr),(str,arr),(str,arr)...],
    #   2:[(str,arr),(str,arr),(str,arr)...],
    #   ...}
    clusters = {}

    for data in dataSet:
        dist = [np.sqrt(np.sum((data[1] - centroid) ** 2)) for centroid in centroids]
        k = dist.index(min(dist))
        if k in clusters:
            clusters[k].append(data)
        else:
            clusters[k] = [data]

    return clusters


# Update centroids based on given clusters
def reevaluate_centroids(clusters):
    centroids = []
    keys = sorted(clusters.keys())
    for k in keys:
        features = np.array([tuple_val[1] for tuple_val in clusters[k]])
        centroids.append(np.mean(features, axis=0))
    return np.array(centroids)
